<?php

/**
 * Implements hook_preprocess_node().
 */
function demot_preprocess_node(&$variables) {
  if ($variables['type'] == 'article' && $variables['view_mode'] == 'buy_item') {
    $variables['display_submitted'] = false;
  }
}

/**
 * Implements hook_preprocess_buy_list().
 */
function demot_preprocess_buy_list(&$variables) {
  $variables['classes_array'][] = 'pure-g';
}

/**
 * Implements hook_preprocess_page().
 */
function demot_preprocess_page(&$variables) {
  $variables['page']['top_menu'] = array('#theme' => 'top_menu');
}

/**
 * Implements hook_preprocess_page().
 */
function demot_preprocess_form(&$variables) {
  $variables['element']['#attributes']['class'][] = 'pure-form';
}

/**
 * Implements hook_preprocess_page().
 */
function demot_preprocess_button(&$variables) {
  $variables['element']['#attributes']['class'][] = 'pure-button';
  if (!empty($variables['element']['#attributes']['disabled'])) {
    $variables['element']['#attributes']['class'][] = 'pure-button-disabled';
  }
}

/**
 * Implements hook_theme()
 */
function demot_theme() {
  return array(
    'top_menu' => array(
      'variables' => array(
        'first_item_title' => t('Services'),
        'firt_item_first_column_menu' => 'navigation',
        'firt_item_second_column_menu' => 'navigation',
        'second_item_logout_state_link' => NULL,
        'second_item_logged_in_state_title' => NULL,
        'second_item_logged_in_state_menu' => 'navigation',
      ),
      'template' => 'top-menu',
      'path' => drupal_get_path('theme', 'demot') . '/templates',
    ),
  );
}

function demot_preprocess_top_menu(&$variables) {
  if ($variables['logged_in'] = user_is_logged_in()) {
    global $user;
    $variables['second_item_logged_in_state_title'] = t('Welcome, @user_name', array('@user_name' => $user->name));

    if ($variables['second_item_logged_in_state_menu']) {
      $tree = menu_tree_page_data($variables['second_item_logged_in_state_menu']);
      $variables['second_item_logged_in_state_menu_output'][$menu_name] = menu_tree_output($tree);
    }
    $variables['member_since_info'] = 'Member since: October 16 2015';
  }
  else {
    $variables['second_item_logout_state_link'] = array(
      '#theme' => 'link',
      '#text' => t('Register / Login'),
      '#path' => 'user',
      '#options' => array(
        'attributes' => array('class' => array('ninetenth-link'), 'id' => 'cool-id'),
       ),
    );
  }
  if ($variables['firt_item_first_column_menu']) {
    $tree = menu_tree_page_data($variables['firt_item_first_column_menu']);
    $variables['firt_item_first_column_menu_output'][$menu_name] = menu_tree_output($tree);
  }
  if ($variables['firt_item_second_column_menu']) {
    $tree = menu_tree_page_data($variables['firt_item_second_column_menu']);
    $variables['firt_item_second_column_menu_output'][$menu_name] = menu_tree_output($tree);
  }
}
