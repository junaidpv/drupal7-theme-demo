<div>
  <ul>
    <li>
      <a href="#"><?php print $first_item_title; ?></a>
      <ul>
        <li>
          <?php print render($firt_item_first_column_menu_output); ?>
          <?php print render($firt_item_second_column_menu_output); ?>
        </li>
      </ul>
    </li>
    <li>
      <?php if ($logged_in): ?>
        <a href="#"><?php print $second_item_logged_in_state_title; ?></a>
        <p><?php print $member_since_info; ?></p>
        <?php print render($second_item_logged_in_state_menu_output); ?>
      <?php else: ?>
        <?php print render($second_item_logout_state_link); ?>
      <?php endif; ?>
    </li>
  </ul>

</div>
