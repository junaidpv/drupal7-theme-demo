<div class="<?php print $classes; ?>">
  <?php foreach ($items as $item): ?>
    <?php print render($item); ?>
  <?php endforeach; ?>
</div>
